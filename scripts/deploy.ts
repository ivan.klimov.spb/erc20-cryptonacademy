import { ethers } from "hardhat";
import { IVKLIMToken } from "../src/types";

async function main() {
  const factory = await ethers.getContractFactory("IVKLIMToken");
  console.log("Deploying IVKLIMToken...");

  const contract: IVKLIMToken = await factory.deploy("IVKLIM", "KLIM", 18);

  await contract.deployed();

  console.log("IVKLIMToken deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
