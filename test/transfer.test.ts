import { expect, assert } from "chai";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { IVKLIMToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const DECIMALS = 18;
const MINT_VALUE: BigNumber = BigNumber.from(1000);

describe("Transfer", function () {
  let token: IVKLIMToken;
  let accounts: SignerWithAddress[];

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const factory = await ethers.getContractFactory("IVKLIMToken", accounts[2]);
    token = await factory.deploy(NAME, SYMBOL, DECIMALS);
    await token.deployed();
  });

  it("Balance from account[1] eq 0 before transfer account[0]", async () => {
    const balanceOf: BigNumber = await token.balanceOf(accounts[1].address);
    assert.equal(balanceOf.toNumber(), 0);
  });

  it("Balance account[1] increases by 1000 after transfer from account[0]", async () => {
    await token.mint(accounts[0].address, MINT_VALUE);

    const balanceBefore = await token.balanceOf(accounts[1].address);
    await token.connect(accounts[0]).transfer(accounts[1].address, MINT_VALUE);
    const balanceAfter = await token.balanceOf(accounts[1].address);

    const result = balanceAfter.sub(balanceBefore);
    assert.deepEqual(result, MINT_VALUE);
  });

  describe("Emit", function () {
    it("Emit Transfer(address[0], address[1], 1000)", async () => {
      await token.mint(accounts[0].address, MINT_VALUE);

      await expect(
        token.connect(accounts[0]).transfer(accounts[1].address, MINT_VALUE)
      )
        .to.emit(token, "Transfer")
        .withArgs(accounts[0].address, accounts[1].address, MINT_VALUE);
    });
  });

  describe("Reverted", function () {
    it('transfer address "to" cannot be zero', async () => {
      await token.mint(accounts[0].address, MINT_VALUE);
      await expect(
        token
          .connect(accounts[0])
          .transfer(ethers.constants.AddressZero, MINT_VALUE)
      ).to.be.revertedWith("IVKLIMToken: address in transfer cannot be zero");
    });

    it("The amount exceeds the balance", async () => {
      await expect(
        token.connect(accounts[0]).transfer(accounts[1].address, MINT_VALUE)
      ).to.be.revertedWith("IVKLIMToken: the amount exceeds the balance");
    });
  });
});
