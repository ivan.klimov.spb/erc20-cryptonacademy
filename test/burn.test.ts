import { expect, assert } from "chai";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { IVKLIMToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const DECIMALS = 18;
const MINT_VALUE: BigNumber = BigNumber.from(1000);
const BURN_VALUE: BigNumber = BigNumber.from(1000);

describe("Burn", function () {
  let token: IVKLIMToken;
  let accounts: SignerWithAddress[];

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const factory = await ethers.getContractFactory("IVKLIMToken", accounts[2]);
    token = await factory.deploy(NAME, SYMBOL, DECIMALS);
    await token.deployed();
  });

  it("balance owner decreases by 1000 after burn 1000 for sender account", async () => {
    await token.mint(accounts[0].address, MINT_VALUE);

    const balanceBefore = await token.balanceOf(accounts[0].address);
    await token.connect(accounts[0]).burn(accounts[0].address, BURN_VALUE);
    const balanceAfter = await token.balanceOf(accounts[0].address);

    const result = balanceAfter.sub(balanceBefore);
    assert.deepEqual(result, BURN_VALUE.mul(-1));
  });

  it("Total supply decreases by 1000 after burn 1000", async () => {
    await token.mint(accounts[0].address, MINT_VALUE);
    const totalSupplyBefore = await token.totalSupply();
    await token.connect(accounts[0]).burn(accounts[0].address, BURN_VALUE);
    const totalSupplyAfter = await token.totalSupply();

    const result = totalSupplyAfter.sub(totalSupplyBefore);
    assert.deepEqual(result, BURN_VALUE.mul(-1));
  });

  describe("Emit", function () {
    it("Emit Transfer(address[1], AddressZero, 1000)", async () => {
      await token.mint(accounts[1].address, MINT_VALUE);
      await expect(
        token.connect(accounts[1]).burn(accounts[1].address, BURN_VALUE)
      )
        .to.emit(token, "Transfer")
        .withArgs(
          accounts[1].address,
          ethers.constants.AddressZero,
          BURN_VALUE
        );
    });
  });

  describe("Reverted", function () {
    it("Only msgSender can burn", async () => {
      await token.mint(accounts[1].address, MINT_VALUE);
      await expect(
        token.burn(accounts[1].address, BURN_VALUE)
      ).to.be.revertedWith("IVKLIMToken: only msgSender can burn");
    });

    it("Not enough balance to burn", async () => {
      await expect(
        token.connect(accounts[1]).burn(accounts[1].address, BURN_VALUE)
      ).to.be.revertedWith("IVKLIMToken: not enough balance to burn");
    });
  });
});
