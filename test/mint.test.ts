import { expect, assert } from "chai";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { IVKLIMToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const DECIMALS = 18;
const MINT_VALUE: BigNumber = BigNumber.from(1000);

describe("Mint", function () {
  let token: IVKLIMToken;
  let accounts: SignerWithAddress[];

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const factory = await ethers.getContractFactory("IVKLIMToken", accounts[2]);
    token = await factory.deploy(NAME, SYMBOL, DECIMALS);
    await token.deployed();
  });

  it("balance owner increases by 1000 after Mint 1000 for owner account", async () => {
    const balanceBefore = await token.balanceOf(accounts[0].address);
    await token.mint(accounts[0].address, MINT_VALUE);
    const balanceAfter = await token.balanceOf(accounts[0].address);

    const result = balanceAfter.sub(balanceBefore);
    assert.deepEqual(result, MINT_VALUE);
  });

  it("balance account[1] increases by 1000 after Mint 1000 for account[1]", async () => {
    const balanceBefore = await token.balanceOf(accounts[1].address);
    await token.mint(accounts[1].address, MINT_VALUE);
    const balanceAfter = await token.balanceOf(accounts[1].address);

    const result = balanceAfter.sub(balanceBefore);
    assert.deepEqual(result, MINT_VALUE);
  });

  it("balance account[1] increases by 1000 after Mint 1000 for account[1]", async () => {
    const balanceBefore = await token.balanceOf(accounts[1].address);
    await token.mint(accounts[1].address, MINT_VALUE);
    const balanceAfter = await token.balanceOf(accounts[1].address);

    const result = balanceAfter.sub(balanceBefore);
    assert.deepEqual(result, MINT_VALUE);
  });

  it("Total supply increases  by 1000 after Mint 1000", async () => {
    const totalSupplyBefore = await token.totalSupply();
    await token.mint(accounts[1].address, MINT_VALUE);
    const totalSupplyAfter = await token.totalSupply();

    const result = totalSupplyAfter.sub(totalSupplyBefore);
    assert.deepEqual(result, MINT_VALUE);
  });

  describe("Emit", function () {
    it("Emit Transfer(AddressZero, address[1], 1000)", async () => {
      await expect(token.mint(accounts[1].address, MINT_VALUE))
        .to.emit(token, "Transfer")
        .withArgs(
          ethers.constants.AddressZero,
          accounts[1].address,
          MINT_VALUE
        );
    });
  });

  describe("Reverted", function () {
    it("Mint to zero address cannot be", async () => {
      await expect(
        token.mint(ethers.constants.AddressZero, MINT_VALUE)
      ).to.be.revertedWith('IVKLIMToken: address mint "to" cannot be zero');
    });

    it("Only owner can mint", async () => {
      await expect(
        token.connect(accounts[1]).mint(accounts[1].address, MINT_VALUE)
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });
  });
});
