import { expect, assert } from "chai";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { IVKLIMToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const DECIMALS = 18;
const MINT_VALUE: BigNumber = BigNumber.from(1000);

describe("Transfer from", function () {
  let token: IVKLIMToken;
  let accounts: SignerWithAddress[];

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const factory = await ethers.getContractFactory("IVKLIMToken", accounts[2]);
    token = await factory.deploy(NAME, SYMBOL, DECIMALS);
    await token.deployed();
  });

  it("Balance from account[1] eq 0 before transfer from account[0]", async () => {
    const balanceOf: BigNumber = await token.balanceOf(accounts[1].address);
    assert.equal(balanceOf.toNumber(), 0);
  });

  it("Balance account[1] increases by 1000 after transfer from account[0]", async () => {
    await token.mint(accounts[0].address, MINT_VALUE);
    await token.connect(accounts[0]).approve(accounts[1].address, MINT_VALUE);

    const balanceBefore = await token.balanceOf(accounts[1].address);
    await token
      .connect(accounts[1])
      .transferFrom(accounts[0].address, accounts[1].address, MINT_VALUE);
    const balanceAfter = await token.balanceOf(accounts[1].address);

    const result = balanceAfter.sub(balanceBefore);
    assert.deepEqual(result, MINT_VALUE);
  });

  it("Balance account[1] increases by 1000 after transfer from account[0] with max allowance", async () => {
    await token.mint(accounts[0].address, MINT_VALUE);
    await token
      .connect(accounts[0])
      .approve(accounts[1].address, ethers.constants.MaxUint256);

    const balanceBefore = await token.balanceOf(accounts[1].address);
    await token
      .connect(accounts[1])
      .transferFrom(accounts[0].address, accounts[1].address, MINT_VALUE);
    const balanceAfter = await token.balanceOf(accounts[1].address);

    const result = balanceAfter.sub(balanceBefore);
    assert.deepEqual(result, MINT_VALUE);
  });

  it("Balance account[1] increases by 1000 after transfer from account[0] from sender account[0]", async () => {
    await token.mint(accounts[0].address, MINT_VALUE);

    const balanceBefore = await token.balanceOf(accounts[1].address);
    await token
      .connect(accounts[0])
      .transferFrom(accounts[0].address, accounts[1].address, MINT_VALUE);
    const balanceAfter = await token.balanceOf(accounts[1].address);

    const result = balanceAfter.sub(balanceBefore);
    assert.deepEqual(result, MINT_VALUE);
  });

  describe("Emit", function () {
    it("Emit Transfer(address[0], address[1], 1000)", async () => {
      await token.mint(accounts[0].address, MINT_VALUE);
      await token.connect(accounts[0]).approve(accounts[1].address, MINT_VALUE);

      await expect(
        token
          .connect(accounts[1])
          .transferFrom(accounts[0].address, accounts[1].address, MINT_VALUE)
      )
        .to.emit(token, "Transfer")
        .withArgs(accounts[0].address, accounts[1].address, MINT_VALUE);
    });
  });

  describe("Reverted", function () {
    it("Insufficient allowance", async () => {
      await token.mint(accounts[0].address, MINT_VALUE);
      await expect(
        token
          .connect(accounts[1])
          .transferFrom(accounts[0].address, accounts[1].address, MINT_VALUE)
      ).to.be.revertedWith("IVKLIMToken: insufficient allowance");
    });

    it("Address from cannot be zero", async () => {
      await expect(
        token
          .connect(accounts[0])
          .transferFrom(ethers.constants.AddressZero, accounts[1].address, 0)
      ).to.be.revertedWith(
        'IVKLIMToken: approve address "from" cannot be zero'
      );
    });
  });
});
