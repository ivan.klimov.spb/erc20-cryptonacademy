const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { IVKLIMToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const DECIMALS = 18;

describe("Deployment ", function () {
  let token: IVKLIMToken;
  let accounts: SignerWithAddress[];

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const factory = await ethers.getContractFactory("IVKLIMToken", accounts[0]);
    token = await factory.deploy(NAME, SYMBOL, DECIMALS);
    await token.deployed();
  });

  it("Should set the right owner", async function () {
    expect(await token.owner()).to.equal(accounts[0].address);
  });

  describe("Check token info after deployment", function () {
    it("After deploy name must be IVKLIM", async () => {
      const name: string = await token.name();
      assert.equal(name, NAME);
    });

    it("After deploy symbol must be KLIM", async () => {
      const symbol: string = await token.symbol();
      assert.equal(symbol, SYMBOL);
    });

    it("After deploy decimals must be 18", async () => {
      const decimals: number = await token.decimals();
      assert.equal(decimals, DECIMALS);
    });

    it("After deploy totalSupply must be 0", async () => {
      const totalSupply: BigNumber = await token.totalSupply();
      assert.equal(totalSupply, 0);
    });
  });
});
