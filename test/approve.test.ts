import { expect, assert } from "chai";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { IVKLIMToken } from "../src/types";
import { BigNumber } from "ethers";

const NAME: string = "IVKLIM";
const SYMBOL: string = "KLIM";
const DECIMALS = 18;

describe("Approve", function () {
  let token: IVKLIMToken;
  let accounts: SignerWithAddress[];

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    const factory = await ethers.getContractFactory("IVKLIMToken", accounts[2]);
    token = await factory.deploy(NAME, SYMBOL, DECIMALS);
    await token.deployed();
  });

  it("Allowance eq  0 for account[1] before approve account[0]", async () => {
    const allowance: BigNumber = await token.allowance(
      accounts[0].address,
      accounts[1].address
    );
    assert.equal(allowance.toNumber(), 0);
  });

  it("Allowance eq 1000 for account[1] after approve 1000 account[0]", async () => {
    await token.connect(accounts[0]).approve(accounts[1].address, 1000);

    const allowance: BigNumber = await token
      .connect(accounts[0])
      .allowance(accounts[0].address, accounts[1].address);
    assert.equal(allowance.toNumber(), 1000);
  });
  describe("Emit", function () {
    it("Emit Approval ( account[0], account[1], 1000) for account[1] after approve 1000 account[0]", async () => {
      const value: BigNumber = BigNumber.from(1000);

      await expect(
        token.connect(accounts[0]).approve(accounts[1].address, value)
      )
        .to.emit(token, "Approval")
        .withArgs(accounts[0].address, accounts[1].address, value);
    });
  });
  describe("Reverted", function () {
    it("Approve address to cannot be zero", async () => {
      const value: BigNumber = BigNumber.from(1000);

      await expect(
        token.approve(ethers.constants.AddressZero, value)
      ).to.be.revertedWith(
        'IVKLIMToken: approve address "spender" cannot be zero'
      );
    });
  });
});
