import { task } from "hardhat/config";
import { IVKLIMToken } from "../src/types";
import { TASK_TRANSFER } from "./task-names";

task(TASK_TRANSFER, "Transfer tokens")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("spender", "Address of the account that will receive the tokens")
  .addParam("value", "Amount of tokens")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);
    const factory = await hre.ethers.getContractFactory("IVKLIMToken", account);

    let token = (await new hre.ethers.Contract(
      args.contract,
      factory.interface,
      account
    )) as IVKLIMToken;

    await token.transfer(args.spender, BigInt(args.value));
  });
