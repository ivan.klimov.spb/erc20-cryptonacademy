import { task } from "hardhat/config";
import { IVKLIMToken } from "../src/types";
import { TASK_APPROVE } from "./task-names";

task(TASK_APPROVE, "Approve to receive token for account")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam(
    "spender",
    "Address of the account that will can receive the tokens"
  )
  .addParam("value", "Amount of tokens")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);
    const factory = await hre.ethers.getContractFactory("IVKLIMToken", account);

    let token = (await new hre.ethers.Contract(
      args.contract,
      factory.interface,
      account
    )) as IVKLIMToken;

    await token.approve(args.spender, BigInt(args.value));
  });
