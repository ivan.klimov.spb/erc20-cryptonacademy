import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { subtask, task } from "hardhat/config";
import { IVKLIMToken } from "../src/types";
import { TASK_ALLOWANCE } from "./task-names";

task(TASK_ALLOWANCE, "Get allowance")
  .addParam("contract", "contract address")
  .addParam("owner", "Tokens owner")
  .addParam("spender", "Account that can receive tokens")
  .setAction(async (args, hre) => {
    let account: SignerWithAddress[] = await hre.ethers.getSigners();
    const factory = await hre.ethers.getContractFactory(
      "IVKLIMToken",
      account[0]
    );

    let token = (await new hre.ethers.Contract(
      args.contract,
      factory.interface,
      account[0]
    )) as IVKLIMToken;

    const allowance = await token.allowance(args.owner, args.spender);
    console.log("owner allowance for spender: " + allowance);
  });
