import { task } from "hardhat/config";
import { IVKLIMToken } from "../src/types";
import { TASK_TRANSFER_FROM } from "./task-names";

task(TASK_TRANSFER_FROM, "Transfer tokens from account to account")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("from", "From this account will send tokens")
  .addParam("to", "This account will receive tokens")
  .addParam("value", "Amount of tokens")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);
    const factory = await hre.ethers.getContractFactory("IVKLIMToken", account);

    let token = (await new hre.ethers.Contract(
      args.contract,
      factory.interface,
      account
    )) as IVKLIMToken;

    await token.transferFrom(args.from, args.to, BigInt(args.value));
  });
