import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { subtask, task } from "hardhat/config";
import { IVKLIMToken } from "../src/types";
import { TASK_BALANCE_OF } from "./task-names";

task(TASK_BALANCE_OF, "Get balanceOf")
  .addParam("contract", "contract address")
  .addParam("owner", "Tokens owner")
  .setAction(async (args, hre) => {
    let account: SignerWithAddress[] = await hre.ethers.getSigners();
    const factory = await hre.ethers.getContractFactory(
      "IVKLIMToken",
      account[0]
    );

    let token = (await new hre.ethers.Contract(
      args.contract,
      factory.interface,
      account[0]
    )) as IVKLIMToken;

    const balanceOf = await token.balanceOf(args.owner);
    console.log("balance Of owner: " + balanceOf);
  });
