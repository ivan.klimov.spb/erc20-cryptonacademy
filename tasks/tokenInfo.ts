import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { subtask, task } from "hardhat/config";
import { IVKLIMToken } from "../src/types";
import {
  TASK_NAME,
  TASK_SYMBOL,
  TASK_DECIMALS,
  TASK_TOTAL_SUPPLY,
  TASK_INIT_VIEW,
} from "./task-names";

subtask(TASK_INIT_VIEW, "Init token")
  .addParam("contract", "contract address")
  .setAction(async (args, hre) => {
    let account: SignerWithAddress[] = await hre.ethers.getSigners();
    const factory = await hre.ethers.getContractFactory(
      "IVKLIMToken",
      account[0]
    );

    let token = (await new hre.ethers.Contract(
      args.contract,
      factory.interface,
      account[0]
    )) as IVKLIMToken;

    return token;
  });

task(TASK_NAME, "Get token name")
  .addParam("contract", "contract address")
  .setAction(async (args, hre) => {
    let token: IVKLIMToken = await hre.run(TASK_INIT_VIEW);

    const name = await token.name();
    console.log("name: " + name);
  });

task(TASK_SYMBOL, "Get token symbol")
  .addParam("contract", "contract address")
  .setAction(async (args, hre) => {
    let token: IVKLIMToken = await hre.run(TASK_INIT_VIEW);

    const symbol = await token.symbol();
    console.log("symbol: " + symbol);
  });

task(TASK_DECIMALS, "Get token decimals")
  .addParam("contract", "contract address")
  .setAction(async (args, hre) => {
    let token: IVKLIMToken = await hre.run(TASK_INIT_VIEW);

    const decimals = await token.decimals();
    console.log("decimals: " + decimals);
  });

task(TASK_TOTAL_SUPPLY, "Get token total supply")
  .addParam("contract", "contract address")
  .setAction(async (args, hre) => {
    let token: IVKLIMToken = await hre.run(TASK_INIT_VIEW);

    const totalSupply = await token.totalSupply();
    console.log("decimals: " + totalSupply);
  });
