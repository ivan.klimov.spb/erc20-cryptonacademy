export const TASK_NAME = "name";
export const TASK_SYMBOL = "symbol";
export const TASK_DECIMALS = "decimals";
export const TASK_TOTAL_SUPPLY = "totalSupply";
export const TASK_ALLOWANCE = "allowance";
export const TASK_BALANCE_OF = "balanceOf";

export const TASK_APPROVE = "approve";
export const TASK_TRANSFER_FROM = "transferFrom";
export const TASK_TRANSFER = "transfer";

export const TASK_MINT = "mint";
export const TASK_BURN = "birn";

export const TASK_INIT_VIEW = "initView";
