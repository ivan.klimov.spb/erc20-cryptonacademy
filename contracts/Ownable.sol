pragma solidity >=0.7.0 <0.9.0;

// SPDX-License-Identifier: MIT
import "./Context.sol";

abstract contract Ownable is Context {
    address private currentOwner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    constructor() {
        _transferOwnership(_msgSender());
    }

    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }

    function  owner() public view returns(address ownerAddress) {
        return currentOwner;
    }

    function _transferOwnership(address newOwner) internal {
        address oldAddress = owner();
        currentOwner = newOwner;
        emit OwnershipTransferred(oldAddress, currentOwner);
    }
}