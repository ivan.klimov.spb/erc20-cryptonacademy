/**
 * @type import('hardhat/config').HardhatUserConfig
 */
import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });
import "@typechain/hardhat";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-waffle";
import "solidity-coverage";
import "@nomiclabs/hardhat-ganache";
import "tsconfig-paths/register";
import "@nomiclabs/hardhat-etherscan";

import "./tasks/tokenInfo";
import "./tasks/allowance";
import "./tasks/balance-of";

import "./tasks/approve";
import "./tasks/transfer-from";
import "./tasks/transfer";

import "./tasks/mint";
import "./tasks/burn";

const { API_URL, PRIVATE_KEY, PRIVATE_KEY2, PRIVATE_KEY3, API_KEY } =
  process.env;

module.exports = {
  solidity: "0.8.1",
  networks: {
    ganache: {
      url: "http://127.0.0.1:7545",
      //chainId: 1287, // 0x507 in hex,
      accounts: [PRIVATE_KEY, PRIVATE_KEY2, PRIVATE_KEY3],
    },
    rinkeby: {
      url: API_URL,
      accounts: [PRIVATE_KEY, PRIVATE_KEY2, PRIVATE_KEY3],
    },
  },
  typechain: {
    outDir: "src/types",
    target: "ethers-v5",
    alwaysGenerateOverloads: false, // should overloads with full signatures like deposit(uint256) be generated always, even if there are no overloads?
    externalArtifacts: ["externalArtifacts/*.json"], // optional array of glob patterns with external artifacts to process (for example external libs from node_modules)
  },
  etherscan: {
    // Your API key for Etherscan
    // Obtain one at https://etherscan.io/
    apiKey: API_KEY,
  },
};
