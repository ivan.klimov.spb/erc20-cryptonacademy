#Do this first

Add your file .env to root project

Structure .env
```
PRIVATE_KEY = string
PUBLIC_KEY =  string
PRIVATE_KEY2 = string
PUBLIC_KEY2 = string
PRIVATE_KEY3 = string
PUBLIC_KEY3 = string
API_URL = string
```

#Start
```
npm install
```
```
npx hardhat compile
```

#Run test
```
npx hardhat coverage
```

#Deploy contract to rinkeby via alchemy
```
npx hardhat run scripts/deploy.ts --network rinkeby  
```
#Verify contract 
```
npx hardhat verify --network rinkeby CONTRACT_ADDRESS "IVKLIM" "KLIM" 18 
```

#Look at tasks list
```
npx hardhat  --help
```
